# run the task specified and save false negative, false positive, correct positive
# CAVEATE : no space in task name (GlaucomatousDiscChange)
import utils
import os
import argparse
import time
import iterator_labels
from keras.models import model_from_json
import label_provider
import numpy as np
import keras.backend as K
import tensorflow as tf

from keras import backend as K
from keras import objectives
from keras.layers import Input
from keras.layers import Conv2D, MaxPooling2D, GlobalAveragePooling2D, Dense
from keras.models import Model
from keras import regularizers, initializers
from keras.layers.advanced_activations import ELU
from keras.layers.core import Lambda

# arrange arguments
parser = argparse.ArgumentParser()
parser.add_argument(
    '--gpu_index',
    type=str,
    required=True
    )
FLAGS, _ = parser.parse_known_args()

# set gpu 
os.environ['CUDA_VISIBLE_DEVICES'] = FLAGS.gpu_index

# set system for validation
config_dict = utils.inference_config()
path_img_dir_template = "/media/ext/resized/system{}/"
systems_validation = [3]

# training settings
validation_filenames_labels = label_provider.labels(systems_validation, 1, 3, None, path_img_dir_template, option="good_mediaopacity")
validation_batch_fetcher = iterator_labels.ValidationBatchFetcher(validation_filenames_labels, 32, "normalize")

# load a network
model_dir = "../model_good_mediaopacity"
network_file = utils.all_files_under(model_dir, extension=".json")
weight_file = utils.all_files_under(model_dir, extension=".h5")
assert len(network_file) == 1 and len(weight_file) == 1
with open(network_file[0], 'r') as f:
    network = model_from_json(f.read())
network.load_weights(weight_file[0])

# evaluate on validation set
start_time = time.time()
pred_labels = []
true_labels = []
filenames = []
for  fns, batch_x, batch_y in validation_batch_fetcher():
    preds = network.predict(batch_x)
    filenames += fns.tolist()
    preds = np.round(network.predict(batch_x)[:, 0])
    pred_labels += preds.tolist()
    true_labels += batch_y.tolist()

utils.print_stats_category(true_labels, pred_labels)
utils.analyze_imgs_image_quality(filenames, true_labels, pred_labels, "../img_analyses_good_mediaopacity")
duration = time.time() - start_time
print "duration : {}".format(duration)
