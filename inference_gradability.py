# run the task specified and save false negative, false positive, correct positive
# CAVEATE : no space in task name (GlaucomatousDiscChange)
import utils
import os
import argparse
import time
import iterator_labels
from keras.models import model_from_json
import label_provider
import numpy as np
import keras.backend as K
import tensorflow as tf

from keras import backend as K
from keras import objectives
from keras.layers import Input
from keras.layers import Conv2D, MaxPooling2D, GlobalAveragePooling2D, Dense
from keras.models import Model
from keras import regularizers, initializers
from keras.layers.advanced_activations import ELU
from keras.layers.core import Lambda


def selu_block(x, n_filters, kernel_size, stride, l2_coeff, padding):
    alpha = 1.6732632423543772848170429916717
    scale = 1.0507009873554804934193349852946    
    k = kernel_size
    s = stride
    
    x = Conv2D(n_filters, (k, k), strides=(s, s), padding=padding,
                   kernel_regularizer=regularizers.l2(l2_coeff), bias_regularizer=regularizers.l2(l2_coeff))(x)
    x = ELU(alpha=alpha)(x)
    x = Lambda(lambda x:scale * x)(x)
    
    return x


def network_gradability():
    """
    generate network based on unet
    """    
        
    # set image specifics
    n_filters = 32
    out_ch = 4
    k = 3  # kernel size
    s = 2  # stride
    img_h, img_w, img_ch = 512, 512, 3
    padding = 'same'
    l2_coeff = 0.0005
    
    inputs = Input((img_h, img_w, img_ch))
    last_layer = inputs
    for i in range(5):
        if i < 2:
            x = selu_block(last_layer, n_filters * (2 ** i), k, s, l2_coeff, padding)
        else:
            x = selu_block(last_layer, n_filters * (2 ** i), k, 1, l2_coeff, padding)
        if i != 4:
            x = MaxPooling2D(pool_size=(s, s))(x)
        last_layer = x
    
    gap = GlobalAveragePooling2D()(last_layer)
    outputs = Dense(out_ch, activation='softmax', kernel_regularizer=regularizers.l2(l2_coeff), bias_regularizer=regularizers.l2(l2_coeff))(gap)
    
    model = Model(inputs, outputs)    

    return model 


# arrange arguments
parser = argparse.ArgumentParser()
parser.add_argument(
    '--gpu_index',
    type=str,
    required=True
    )
parser.add_argument(
    '--batch_size',
    type=int,
    help="batch size",
    required=True
    )
parser.add_argument(
    '--consensus_level',
    type=int,
    required=True
    )
FLAGS, _ = parser.parse_known_args()

# set gpu 
os.environ['CUDA_VISIBLE_DEVICES'] = FLAGS.gpu_index

# set system for validation
config_dict = utils.inference_config()
path_img_dir_template = "/media/ext/resized/system{}/"
systems_validation = [3]

# training settings
validation_filenames_labels = label_provider.labels(systems_validation, 1, FLAGS.consensus_level, 3, path_img_dir_template)
validation_batch_fetcher = iterator_labels.ValidationBatchFetcher(validation_filenames_labels, FLAGS.batch_size, "normalize")
print validation_filenames_labels["label"].value_counts(normalize=False)

# load a network
model_dir = "../model_for_inference/gradability"
weight_file = utils.all_files_under(model_dir, extension=".h5")
assert len(weight_file) == 1
network = network_gradability()
network.load_weights(weight_file[0])

# evaluate on validation set
start_time = time.time()
pred_labels = []
true_labels = []
filenames = []
for  fns, batch_x, batch_y in validation_batch_fetcher():
    preds = network.predict(batch_x)
    filenames += fns.tolist()
    pred_labels += np.argmax(network.predict(batch_x), axis=1).tolist()
    true_labels += np.argmax(batch_y, axis=1).tolist()

utils.print_stats_category(true_labels, pred_labels)
utils.analyze_imgs_image_quality(filenames, true_labels, pred_labels, "../img_analyses_img_quality")
duration = time.time() - start_time
print "duration : {}".format(duration)
