# run the task specified and save false negative, false positive, correct positive
# CAVEATE : no space in task name (GlaucomatousDiscChange)
#         : region information can not be trusted
import utils
import os
import argparse
import time
import iterator
from keras.models import model_from_json
import label_provider
import numpy as np
import keras.backend as K
import tensorflow as tf

# arrange arguments
parser = argparse.ArgumentParser()
parser.add_argument(
    '--gpu_index',
    type=str,
    required=True
    )
parser.add_argument(
    '--batch_size',
    type=int,
    help="batch size",
    required=True
    )
FLAGS, _ = parser.parse_known_args()

# set gpu 
os.environ['CUDA_VISIBLE_DEVICES'] = FLAGS.gpu_index

# set misc paths
# dir_inference_results = "../inference_results_FDA/{}_messidor_consensus3"
# img_anal_dir_template = "../img_analyses/{}_messidor_adjudicated_FDA_model".format(FLAGS.task)
# if not os.path.isdir(dir_inference_results):
#     os.makedirs(dir_inference_results)

# training settings
systems_validation = [5]
path_img_dir_template = "/media/ext/resized/system{}/"
validation_filenames_labels = label_provider.labels(systems_validation, 2, 3, 3, path_img_dir_template, option=None)
validation_batch_fetcher = iterator.ValidationBatchFetcher(validation_filenames_labels, FLAGS.batch_size, "normalize")

# load a network
model_dir = "../model_for_inference/normal_abnormal_more_data/"
network = utils.load_network(model_dir)

# evaluate on validation set
start_time = time.time()
pred_labels = []
true_labels = []
filenames = []
activations = []
masks = []

# run inference
for fns, batch_x, batch_y, lm, mask in validation_batch_fetcher():
    filenames += fns.tolist()
    preds, activation = network.predict(batch_x)
    activations.append(activation)
    masks.append(mask)
    pred_labels += preds.tolist()
    true_labels += batch_y.tolist()

# np.save(os.path.join(dir_inference_results, "{}_pred".format(FLAGS.task)), pred_labels)
# np.save(os.path.join(dir_inference_results, "{}_true".format(FLAGS.task)), true_labels)
# activations = np.concatenate(activations, axis=0)
# masks = np.concatenate(masks, axis=0)
utils.print_stats(true_labels, pred_labels)
# utils.analyze_imgs(filenames, true_labels, pred_labels, activations, masks, img_anal_dir_template.format(FLAGS.task))
duration = time.time() - start_time
print "duration : {}".format(duration)
