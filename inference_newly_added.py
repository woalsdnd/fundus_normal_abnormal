# run the task specified and save false negative, false positive, correct positive
# CAVEATE : no space in task name (GlaucomatousDiscChange)
import utils
import os
import argparse
import time
import iterator
from keras.models import model_from_json
import label_provider_newly_added
import numpy as np
import keras.backend as K
import tensorflow as tf

# arrange arguments
parser=argparse.ArgumentParser()
parser.add_argument(
    '--gpu_index',
    type=str,
    required=True
    )
parser.add_argument(
    '--batch_size',
    type=int,
    help="batch size",
    required=True
    )
parser.add_argument(
    '--consensus_level',
    type=int,
    required=True
    )
parser.add_argument(
    '--model',
    type=str,
    required=True
    )
FLAGS,_= parser.parse_known_args()

# set gpu 
os.environ['CUDA_VISIBLE_DEVICES']=FLAGS.gpu_index

# set system for validation
systems_validation = [1, 2, 3, 4]
path_img_dir_template = "/media/ext/resized/system{}/"

# training settings
validation_filenames_labels = label_provider_newly_added.labels(systems_validation, 2, FLAGS.consensus_level, 3, path_img_dir_template, "findings")
validation_batch_fetcher = iterator.ValidationBatchFetcher(validation_filenames_labels, FLAGS.batch_size, "normalize")
print validation_filenames_labels["label"].value_counts(normalize=False)

# load a network
model_dir="../model_for_inference/{}".format(FLAGS.model)
network_file=utils.all_files_under(model_dir,extension=".json")
weight_file=utils.all_files_under(model_dir,extension=".h5")
assert len(network_file)==1 and len(weight_file)==1
with open(network_file[0], 'r') as f:
    network=model_from_json(f.read())
network.load_weights(weight_file[0])

# evaluate on validation set
start_time = time.time()
pred_labels=[]
true_labels=[]
filenames=[]
activations=[]
# k=0
for fns, batch_x, batch_y, lm, mask in validation_batch_fetcher():
#     gradient_function = K.function([network.layers[0].input, K.learning_phase()], [network.layers[-1].output,network.layers[-4].output,network.layers[-5].output])
#     out1, out2, out3 = gradient_function([np.expand_dims(batch_x[0,...], axis=0), 0])
#     print fns[0]
#     print np.max(out1[0,...],axis=(0,1))
#     print np.max(out2[0,...],axis=(0,1))
#     print np.max(out3[0,...],axis=(0,1))
#     k+=1
#     if k==10:
#         exit(1)
    filenames+=fns.tolist()
    preds, activation = network.predict(batch_x)
    activations.append(activation)
    pred_labels+=preds.tolist()
    true_labels+=batch_y.tolist()

activations=np.concatenate(activations,axis=0)
utils.print_stats(true_labels, pred_labels)
utils.analyze_imgs(filenames, true_labels, pred_labels, activations, "../img_analyses/{}".format(FLAGS.model))
duration=time.time()-start_time
print "duration : {}".format(duration)