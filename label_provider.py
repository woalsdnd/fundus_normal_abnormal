import pandas as pd
import os
import sys
import numpy as np

pd.options.mode.chained_assignment = None


def adjust_normal_abnormal(df):
    abnormal_fn = df[(df.level == 2) & (df.question == "Abnormal Fundus")].filename.unique()
    df_lv3 = df[df.level == 3]
    df_lv3.loc[:, "count"] = df_lv3.groupby("filename")["question"].transform('nunique')
    other_findings_fn = df_lv3[(df_lv3.question == "Other Findings or Artifact") & (df_lv3.count == 1)].filename.unique()
    df_lv4 = df[df.level == 4]
    df_lv4.loc[:, "count"] = df_lv4.groupby("filename")["question"].transform('nunique')
    floaters_fn = df_lv4[(df_lv4.question == "Floaters / Artifacts Suspect") & (df_lv4.count == 1)].filename.unique()
    
    df_filtered = df[df.filename.isin(abnormal_fn)]
    df_filtered = df_filtered[df_filtered.filename.isin(other_findings_fn)]
    df_filtered = df_filtered[df_filtered.filename.isin(floaters_fn)]
    df.loc[(df.filename.isin(df_filtered.filename.unique())) & (df.level == 2), "question"] = "Normal Fundus"
    return df


def read(df, n):
    df_lv1 = df[df.level == 1]
    df_lv1 = df_lv1.dropna(subset=["question"])
    df_lv1 = df_lv1.drop_duplicates(subset=["filename", "email", "question"])
    df_lv1.loc[:, "count"] = df_lv1.groupby("filename")["question"].transform('count')
    df = df[df.filename.isin(df_lv1[df_lv1["count"] >= n].filename.unique())]
    return df


def normals(df, n):
    df_lv2 = df[(df.level == 2)]
    df_lv2 = df_lv2.dropna(subset=["question"])
    df_lv2 = df_lv2.drop_duplicates(subset=["filename", "email", "question"])
    df_lv2.loc[:, "count"] = df_lv2[df_lv2.question == "Normal Fundus"].groupby("filename")["question"].transform('count')
    df = df[df.filename.isin(df_lv2[df_lv2["count"] >= n].filename.unique())]
    return df


def gradables(df, n):
    df_lv2 = df[df.level == 2]
    df_lv2 = df_lv2.dropna(subset=["question"])
    df_lv2 = df_lv2.drop_duplicates(subset=["filename", "email", "question"])
    df_lv2.loc[:, "count"] = df_lv2.groupby("filename")["question"].transform('count')
    df = df[df.filename.isin(df_lv2[df_lv2["count"] >= n].filename.unique())]
    return df


def referrable_read(df, n):
    # n reads in step 5
    df_lv5 = df[df.level == 5]
    df_lv5 = df_lv5.dropna(subset=["question"])
    df_lv5 = df_lv5.drop_duplicates(subset=["filename", "email", "question"])
    df_lv5.loc[:, "count"] = df_lv5.groupby("filename")["question"].transform('count')
    df = df[df.filename.isin(df_lv5[df_lv5["count"] >= n].filename.unique())]
    return df


def question(df, level):
    df_level = df[df.level == level]
    df_level = df_level.dropna(subset=["question"])
    df_level = df_level.drop_duplicates(subset=["filename", "email", "question"])
    df_question = df_level.groupby(["filename", "question"]).size().unstack(fill_value=0).reset_index()
    return df_question


def consensus(df, level, consensus_level, consensus_level_normality, option=None):
    df_normal = normals(df, consensus_level_normality)
    df_question = question(df, level)

    if level == 1:
        if option == "gradable_nongradable":
            df_question.loc[:, "Gradable"] = df_question.apply(lambda x:x["Good"] + x["Media Opacity (i.e. cataract)"] + x["Small Pupil / Defocusing"], axis=1)
            df_question.loc[df_question["Gradable"] >= 3, "label"] = 0
            df_question.loc[df_question["Ungradable"] >= 3, "label"] = 1
        elif option == "good_mediaopacity":
            df_question.loc[df_question["Good"] >= 3, "label"] = 0
            df_question.loc[df_question["Media Opacity (i.e. cataract)"] >= 3, "label"] = 1
        elif option == "good_smallpupil":
            df_question.loc[df_question["Good"] >= 3, "label"] = 0
            df_question.loc[df_question["Small Pupil / Defocusing"] >= 3, "label"] = 1
        df_question = df_question.dropna(subset=["label"])
        return df_question[["filename", "label"]]
    elif level == 2:
        if not option:  # check only normality
            df_question.loc[df_question["Normal Fundus"] >= consensus_level, "label"] = 0
            df_question.loc[df_question["Abnormal Fundus"] >= consensus_level, "label"] = 1
            df_question = df_question.dropna(subset=["label"])
            df_question["region"] = ""
            return df_question[["filename", "label", "region"]]
        elif option == "findings":  # check region info
            df_abnormal = df_question[df_question["Abnormal Fundus"] >= consensus_level]
            df.loc[df.filename.isin(df_normal.filename.unique()), "label"] = 0
            df.loc[df.filename.isin(df_abnormal.filename.unique()), "label"] = 1
            # set region info (finding checked but no region => select all region)
            findings = ['Hemorrhage', 'Hard Exudate', 'Cotton Wool Patch',
                            'Drusen & Drusenoid Deposits', 'Retinal Pigmentary Change',
                            'Macular Hole', 'Vascular Abnormality', 'Membrane', 'Fluid Accumulation',
                            'Chroioretinal Atrophy/Scar', 'Choroidal Lesion', 'Myelinated Nerve Fiber',
                            'RNFL Defect', 'Glaucomatous Disc Change', 'Non-glaucomatous Disc Change', 'Other Findings or Artifact']
            df_region = df[np.logical_or.reduce([(df.question == finding) for finding in findings])]
            df_region.loc[pd.isnull(df_region.region), "region"] = "T | ST | IT | SN | IN | SD | ID"
            df_region = df_region.groupby("filename").agg({'region': ' | '.join}).reset_index()
            df = pd.merge(df_region, df[["filename", "label"]], on="filename", how="outer")
            
            # handle exceptional cases 
            df.loc[(df.label == 0) & ~pd.isnull(df.region), "label"] = None  # normal but somehow finding exists => data loss 
            df.loc[(df.label == 1) & pd.isnull(df.region), "region"] = "T | ST | IT | SN | IN | SD | ID"  # no findings checked but abnormal
            
            # clean up labels
            df = df.dropna(subset=["label"])
            df.loc[pd.isnull(df.region), "region"] = ""
            df = df.drop_duplicates(subset=["filename"])
            assert len(df[(df.label == 1) & (df.region == "")]) == 0 and len(df[(df.label == 0) & (df.region != "")]) == 0 and len(df[pd.isnull(df.label)]) == 0 
            return df[["filename", "label", "region"]]
        else:
            return None
    elif level == 3:
        # set region info
        df_region = df[df.question == option].dropna(subset=["region"])
        df_region = df_region.groupby("filename").agg({'region': ' | '.join}).reset_index()
        
        # set label info
        exists = df_question[(df_question[option] >= consensus_level) & df_question.filename.isin((df_region.filename.unique()))].filename.unique()
        notexists = df_question[df_question[option] == 0].filename.unique()
        df = df.drop_duplicates(subset=["filename"])
        df.loc[df.filename.isin(df_normal.filename.unique()) | df.filename.isin(notexists), "label"] = 0
        df.loc[df.filename.isin(exists), "label"] = 1
        df = pd.merge(df_region, df[["filename", "label"]], on="filename", how="outer")
        df.loc[pd.isnull(df.region), "region"] = ""
        df = df.dropna(subset=["label"])
        return df[["filename", "label", "region"]]
    elif level == 4:
        exists = df_question[df_question[option] >= consensus_level].filename.unique()
        notexists = df_question[df_question[option] == 0].filename.unique()
        df = df.drop_duplicates(subset=["filename"])
        df.loc[df.filename.isin(df_normal.filename.unique()) | df.filename.isin(notexists), "label"] = 0
        df.loc[df.filename.isin(exists), "label"] = 1
        df = df.dropna(subset=["label"])
        return df[["filename", "label"]]
    elif level == 5:
        exists = df_question[df_question["Yes (Refer to Ophthalmologist)"] >= consensus_level].filename.unique()
        notexists = df_question[df_question["No"] >= consensus_level].filename.unique()
        df = df.drop_duplicates(subset=["filename"])
        df.loc[df.filename.isin(df_normal.filename.unique()) | df.filename.isin(notexists), "label"] = 0
        df.loc[df.filename.isin(exists), "label"] = 1
        df = df.dropna(subset=["label"])
        return df[["filename", "label"]]
    else:
        assert 1 <= level and level <= 5

    
def append_filepath(path_img_dir_template, df, n_system):
    path_header = path_img_dir_template.format(n_system)
    df.loc[:, "filename"] = df.filename.map(lambda x:os.path.join(path_header, x + ".tiff"))
    return df


def merge_coords_labels(coords, labels):
    print "#abnormal fundus missing landmark pts: {}".format(len(labels[(~labels.filename.isin(coords.filename)) & labels.label == 1].filename.unique()))
    df = pd.merge(coords, labels, on="filename", how="right")
    # remove abnormal but no coords given
    df.loc[(df.label == 1) & (pd.isnull(df.x_disc) | pd.isnull(df.y_disc) | pd.isnull(df.x_fovea) | pd.isnull(df.y_fovea)), "label"] = None 
    df = df.dropna(subset=["label"])
    return df


def print_ratio(perm, n, n_total, threshold=0):
    if 1.*n / n_total > threshold:
        print "{0:<30} : {1} ({2}/{3})".format(perm, 1.*n / n_total, n, n_total)

        
def print_after_sort(df, options, n_total, threshold):
    value_dict = {}
    for option in options:
        value_dict[option] = len(df[df[option] >= threshold])
    list_values = sorted(value_dict.items(), key=lambda x: x[1], reverse=True)
    for (key, val) in list_values:
        print_ratio(key, val, n_total, -1)


def img_quality(df):
    df = df[df.level == 1]
    df = df.dropna(subset=["question"])
    n_total = df.filename.nunique()
    print "***** IMAGE QUALITY ***** [ # dicoms read 3 times : {} ]".format(n_total)
    
    df_question = df.groupby(["filename", "question"]).size().unstack(fill_value=0).reset_index()
    options = df["question"].unique().tolist()
    print "--------------------------- ==3 ---------------------------"
    print_after_sort(df_question, options, n_total, 3)
    print "--------------------------- >=2 ----------------------------"
    print_after_sort(df_question, options, n_total, 2)


def findings(df):
    df = df[df.level == 3]
    df = df.dropna(subset=["question"])
    n_total = df.filename.nunique()
    print "***** FINDINGS ***** [ # dicoms with any findings : {} ]".format(n_total)
    df_question = df.drop_duplicates(subset=["filename", "email", "question"])
    df_question = df_question.groupby(["filename", "question"]).size().unstack(fill_value=0).reset_index()
    options = df["question"].unique().tolist()
    print "--------------------------- ==3 ---------------------------"
    print_after_sort(df_question, options, n_total, 3)
    print "---------------------------- >=2 ----------------------------"
    print_after_sort(df_question, options, n_total, 2)
    print "---------------------------- >=1 ----------------------------"
    print_after_sort(df_question, options, n_total, 1)


def referrable(df):
# for debugging
#     print df[df.filename=="0001_I00150365_1.2.410.200055.999.999.3213476655.13068.1470580416.30492.dcm"]
    df = df[df.level == 5]
    n_total = df.filename.nunique()
    print "***** REFERRABILITY ***** [ # dicoms read 3 times : {} ]".format(n_total)
    
    df_question = df.groupby(["filename", "question"]).size().unstack(fill_value=0).reset_index()
    options = df["question"].unique().tolist()
    print "--------------------------- ==3 ---------------------------"
    print_after_sort(df_question, options, n_total, 3)
    print "---------------------------- >=2 ----------------------------"
    print_after_sort(df_question, options, n_total, 2)


def diagnosis(df):
    df = df[df.level == 4]
    df = df.dropna(subset=["question"])
    n_total = df.filename.nunique()
    print "***** DIAGNOSIS ***** [ # dicoms with any diagnosis: {} ]".format(n_total)
    df_question = df.groupby(["filename", "question"]).size().unstack(fill_value=0).reset_index()
    options = df["question"].unique().tolist()
    print "--------------------------- ==3 ---------------------------"
    print_after_sort(df_question, options, n_total, 3)
    print "---------------------------- >=2 ----------------------------"
    print_after_sort(df_question, options, n_total, 2)
    print "---------------------------- >=1 ----------------------------"
    print_after_sort(df_question, options, n_total, 1)

    
def normality(df):
    df = df[df.level == 2]
    df = df.dropna(subset=["question"])
    n_total = df.filename.nunique()
    print "***** NORMALITY ***** [ # dicoms read 3 times : {} ]".format(n_total)
    
    df_question = df.groupby(["filename", "question"]).size().unstack(fill_value=0).reset_index()
    options = df["question"].unique().tolist()
    print "--------------------------- ==3 ---------------------------"
    print_after_sort(df_question, options, n_total, 3)
    print "--------------------------- >=2 ----------------------------"
    print_after_sort(df_question, options, n_total, 2)


def print_info(df, n_system):
    out_path = "labels_checks_system{}".format(n_system)
    if os.path.exists(out_path):
        os.remove(out_path)
    sys.stdout = open(out_path, 'a')
    print "====================== SYSTEM {} ======================".format(n_system)
    img_quality(df)
    print
    df = gradables(df, 3)
    df = adjust_normal_abnormal(df)
    normality(df)
    print
    findings(df)
    print
    diagnosis(df)
    print
    df = referrable_read(df, 3)
    referrable(df)
    sys.stdout = sys.__stdout__


def labels(list_n_system, level, consensus_level, consensus_level_normality=3, path_img_dir_template=None, option=None):
    list_df = []
    for n_system in list_n_system:
        df = pd.read_csv("/media/ext/merged_labels/system{}_diag.csv".format(n_system))
        df = read(df, 3)
        print_info(df, n_system)
        if level == 1:
            list_df.append(append_filepath(path_img_dir_template, consensus(df, level, consensus_level, consensus_level_normality, option), n_system))
        else:
            df = gradables(df, 3)
            df = adjust_normal_abnormal(df)
            if level == 2:
                if not option:
                    list_df.append(append_filepath(path_img_dir_template, merge_coords_labels(coordinates(n_system), consensus(df, level, consensus_level, consensus_level_normality)), n_system))
                elif option == "findings":
                    list_df.append(append_filepath(path_img_dir_template, merge_coords_labels(coordinates(n_system), consensus(df, level, consensus_level, consensus_level_normality, option)), n_system))
            else:
                if level == 3:
                    assert option != None
                    list_df.append(append_filepath(path_img_dir_template, merge_coords_labels(coordinates(n_system), consensus(df, level, consensus_level, consensus_level_normality, option)), n_system))
                elif level == 4:
                    assert option != None
                    list_df.append(append_filepath(path_img_dir_template, consensus(df, level, consensus_level, consensus_level_normality, option), n_system))
                else:
                    list_df.append(append_filepath(path_img_dir_template, consensus(df, level, consensus_level, consensus_level_normality, option), n_system))
    
    df_final = pd.concat(list_df, ignore_index=True)
    return df_final


def coordinates(n_system):
    df = pd.read_csv("/media/ext/merged_labels/system{}_pts.csv".format(n_system))
    return df
