import os

from keras import backend as K
from keras import objectives
from keras.layers import Input
from keras.layers import Conv2D, MaxPooling2D, GlobalAveragePooling2D, Dense, AveragePooling2D
from keras.models import Model
from keras import regularizers, initializers
from keras.optimizers import Adam, SGD
from keras.layers.advanced_activations import ELU
from keras.layers.core import Lambda, Activation
from keras.layers.normalization import BatchNormalization
from keras.layers.merge import Concatenate, Add
from keras.applications import ResNet50

os.environ['KERAS_BACKEND'] = 'tensorflow'
K.set_image_dim_ordering('tf')


def set_optimizer(network):

    def loss_class(y_true, y_pred):
        return objectives.binary_crossentropy(y_true, y_pred)
    
    def loss_seg(y_true, y_pred):
        return K.mean(K.sum((1 - y_true) * K.log(y_pred + 0.001), axis=3), axis=(1, 2))
    
    network.compile(optimizer=SGD(lr=0, momentum=0.9, nesterov=True), loss=[loss_class, loss_seg], loss_weights=[1, 1], metrics=['accuracy'])

    return network 


def set_optimizer_gradability(network):

    def loss(y_true, y_pred):
        return objectives.categorical_crossentropy(y_true, y_pred)

    network.compile(optimizer=SGD(lr=0, momentum=0.9, nesterov=True), loss=loss, metrics=['accuracy'])
    return network


def conv_block(x, n_filters, filter_size, strides, padding, l2_coeff):
    conv = Conv2D(n_filters, filter_size, strides=strides, padding=padding,
                  kernel_regularizer=regularizers.l2(l2_coeff), bias_regularizer=regularizers.l2(l2_coeff))(x)
    conv = BatchNormalization(scale=False, axis=3)(conv)
    conv = Activation('relu')(conv) 
    return conv


def resnet_buliding_block(x, n_filters, filter_size, padding, l2_coeff):
    conv1 = Conv2D(n_filters, filter_size, padding=padding,
                   kernel_regularizer=regularizers.l2(l2_coeff), bias_regularizer=regularizers.l2(l2_coeff))(x)
    conv1 = BatchNormalization(scale=False, axis=3)(conv1)
    conv1 = Activation('relu')(conv1)   

    conv2 = Conv2D(n_filters, filter_size, padding=padding,
                   kernel_regularizer=regularizers.l2(l2_coeff), bias_regularizer=regularizers.l2(l2_coeff))(conv1)
    conv2 = BatchNormalization(scale=False, axis=3)(conv2)
    conv2 = Add()([conv2, x])
    
    out = Activation('relu')(conv2)   
    
    return out


def resnet_blocks(x, n, n_filters, filter_size, padding, l2_coeff):
    for _ in range(n):
        x = resnet_buliding_block(x, n_filters, filter_size, padding, l2_coeff)
    return x


def resnet50_pretrained():
    resnet = ResNet50(include_top=False, weights='imagenet', input_shape=(512, 512, 3))
    gap = GlobalAveragePooling2D()(resnet.get_layer("activation_49").output)
    outputs = Dense(1, activation="sigmoid")(gap)
    model = Model(resnet.inputs, outputs)
    model.compile(optimizer=SGD(lr=0, momentum=0.9, nesterov=True), loss=objectives.binary_crossentropy, metrics=['accuracy'])
    return model


def network_activation_out(img_shape):

    # set image specifics
    n_filters = 32
    out_ch = 1
    k = 3  # kernel size
    s = 2  # stride
    img_h, img_w, img_ch = img_shape[0], img_shape[1], img_shape[2]
    padding = 'same'
    l2_coeff = 0
    list_n_building_blocks = [2, 3, 4]
    blocks = []
    
    inputs = Input((img_h, img_w, img_ch))
    blocks.append(conv_block(inputs, n_filters, (k, k), (s, s), padding, l2_coeff))
    for index, n_building_blocks in enumerate(list_n_building_blocks):
        blocks.append(resnet_blocks(blocks[index], n_building_blocks, (2 ** index) * n_filters, (k, k), padding, l2_coeff))
        blocks[index + 1] = conv_block(blocks[index + 1], 2 ** (index + 1) * n_filters, (k, k), (s, s), padding, l2_coeff)
    
    list_avg_pools = []
    for i in range(3):
        list_avg_pools.append(AveragePooling2D((8 // (2 ** i), 8 // (2 ** i)))(blocks[i]))
    blocks_concat = Concatenate(axis=3)(list_avg_pools + [blocks[3]])
    list_dilated_conv = []
    list_dilated_conv.append(Conv2D(16 * n_filters, (k, k), padding=padding,
                                    kernel_regularizer=regularizers.l2(l2_coeff), bias_regularizer=regularizers.l2(l2_coeff))(blocks_concat))
    list_dilated_conv.append(Conv2D(16 * n_filters, (k, k), dilation_rate=(2, 2), padding=padding,
                                     kernel_regularizer=regularizers.l2(l2_coeff), bias_regularizer=regularizers.l2(l2_coeff))(blocks_concat))
    list_dilated_conv.append(Conv2D(16 * n_filters, (k, k), dilation_rate=(4, 4), padding=padding,
                                    kernel_regularizer=regularizers.l2(l2_coeff), bias_regularizer=regularizers.l2(l2_coeff))(blocks_concat))
    list_dilated_conv.append(Conv2D(16 * n_filters, (k, k), dilation_rate=(8, 8), padding=padding,
                                    kernel_regularizer=regularizers.l2(l2_coeff), bias_regularizer=regularizers.l2(l2_coeff))(blocks_concat))

    final_block = Concatenate(axis=3)(list_dilated_conv)
    final_block = conv_block(final_block, 16 * n_filters, (k, k), (s, s), padding, l2_coeff)
    final_result = Conv2D(out_ch, (1, 1), padding=padding,  # kernel_initializer=initializers.Constant(1./512), 
                          kernel_regularizer=regularizers.l2(l2_coeff), bias_regularizer=regularizers.l2(l2_coeff))(final_block)
    activation_outs = Activation('sigmoid')(final_result) 
    gap = GlobalAveragePooling2D()(final_result)
    outputs = Activation('sigmoid')(gap)
    
    model = Model(inputs, [outputs, activation_outs])    
    
    def loss_class(y_true, y_pred):
        return objectives.binary_crossentropy(y_true, y_pred)
    
    def loss_seg(y_true, y_pred):
        return K.mean(K.sum((1 - y_true) * K.log(y_pred + 0.001), axis=3), axis=(1, 2))
    
    model.compile(optimizer=SGD(lr=0, momentum=0.9, nesterov=True), loss=[loss_class, loss_seg], loss_weights=[1, 0], metrics=['accuracy'])

    return model 


def finding_network(img_shape):

    # set image specifics
    n_filters = 32
    out_ch = 1
    k = 3  # kernel size
    s = 2  # stride
    img_h, img_w, img_ch = img_shape[0], img_shape[1], img_shape[2]
    padding = 'same'
    l2_coeff = 0
    list_n_building_blocks = [2, 3, 4]
    blocks = []
    
    inputs = Input((img_h, img_w, img_ch))
    blocks.append(conv_block(inputs, n_filters, (k, k), (s, s), padding, l2_coeff))
    for index, n_building_blocks in enumerate(list_n_building_blocks):
        blocks.append(resnet_blocks(blocks[index], n_building_blocks, (2 ** index) * n_filters, (k, k), padding, l2_coeff))
        blocks[index + 1] = conv_block(blocks[index + 1], 2 ** (index + 1) * n_filters, (k, k), (s, s), padding, l2_coeff)
    
    list_avg_pools = []
    for i in range(3):
        list_avg_pools.append(AveragePooling2D((8 // (2 ** i), 8 // (2 ** i)))(blocks[i]))
    blocks_concat = Concatenate(axis=3)(list_avg_pools + [blocks[3]])
    list_dilated_conv = []
    list_dilated_conv.append(Conv2D(16 * n_filters, (k, k), padding=padding,
                                    kernel_regularizer=regularizers.l2(l2_coeff), bias_regularizer=regularizers.l2(l2_coeff))(blocks_concat))
    list_dilated_conv.append(Conv2D(16 * n_filters, (k, k), dilation_rate=(2, 2), padding=padding,
                                     kernel_regularizer=regularizers.l2(l2_coeff), bias_regularizer=regularizers.l2(l2_coeff))(blocks_concat))
    list_dilated_conv.append(Conv2D(16 * n_filters, (k, k), dilation_rate=(4, 4), padding=padding,
                                    kernel_regularizer=regularizers.l2(l2_coeff), bias_regularizer=regularizers.l2(l2_coeff))(blocks_concat))
    list_dilated_conv.append(Conv2D(16 * n_filters, (k, k), dilation_rate=(8, 8), padding=padding,
                                    kernel_regularizer=regularizers.l2(l2_coeff), bias_regularizer=regularizers.l2(l2_coeff))(blocks_concat))

    final_block = Concatenate(axis=3)(list_dilated_conv)
    final_block = conv_block(final_block, 16 * n_filters, (k, k), (s, s), padding, l2_coeff)
    final_result = Conv2D(out_ch, (1, 1), padding=padding,  # kernel_initializer=initializers.Constant(1./512), 
                          kernel_regularizer=regularizers.l2(l2_coeff), bias_regularizer=regularizers.l2(l2_coeff))(final_block)
    gap = GlobalAveragePooling2D()(final_result)
    outputs = Activation('sigmoid')(gap)
    
    model = Model(inputs, outputs)    
    
    def loss_class(y_true, y_pred):
        return objectives.binary_crossentropy(y_true, y_pred)
    
    model.compile(optimizer=SGD(lr=0, momentum=0.9, nesterov=True), loss=loss_class, metrics=['accuracy'])

    return model 


def selu_block(x, n_filters, kernel_size, stride, l2_coeff, padding):
    alpha = 1.6732632423543772848170429916717
    scale = 1.0507009873554804934193349852946    
    k = kernel_size
    s = stride
    
    x = Conv2D(n_filters, (k, k), strides=(s, s), padding=padding,
                   kernel_regularizer=regularizers.l2(l2_coeff), bias_regularizer=regularizers.l2(l2_coeff))(x)
    x = ELU(alpha=alpha)(x)
    x = Lambda(lambda x:scale * x)(x)
    
    return x


def network_gradable_nongradable(img_shape):
        
    # set image specifics
    n_filters = 32
    out_ch = 1
    k = 3  # kernel size
    s = 2  # stride
    img_h, img_w, img_ch = img_shape[0], img_shape[1], img_shape[2]
    padding = 'same'
    l2_coeff = 0
    
    inputs = Input((img_h, img_w, img_ch))
    x = inputs
    for i in range(5):
        if i < 2:
            x = Conv2D(n_filters * (2 ** i), (k, k), strides=(s, s), padding=padding, kernel_regularizer=regularizers.l2(l2_coeff), bias_regularizer=regularizers.l2(l2_coeff))(x)
            x = Activation("relu")(x)
        else:
            x = Conv2D(n_filters * (2 ** i), (k, k), strides=(1, 1), padding=padding, kernel_regularizer=regularizers.l2(l2_coeff), bias_regularizer=regularizers.l2(l2_coeff))(x)
            x = Activation("relu")(x)
        if i != 4:
            x = AveragePooling2D(pool_size=(s, s))(x)
    
    gap = GlobalAveragePooling2D()(x)
    outputs = Dense(out_ch, activation='sigmoid', kernel_regularizer=regularizers.l2(l2_coeff), bias_regularizer=regularizers.l2(l2_coeff))(gap)
    
    model = Model(inputs, outputs)    
    
    model.compile(optimizer=SGD(lr=0, momentum=0.9, nesterov=True), loss=objectives.binary_crossentropy, metrics=['accuracy'])

    return model 


def network_gradability_v4(img_shape):
        
    # set image specifics
    n_filters = 32
    out_ch = 4
    k = 3  # kernel size
    s = 2  # stride
    img_h, img_w, img_ch = img_shape[0], img_shape[1], img_shape[2]
    padding = 'same'
    l2_coeff = 0
    
    inputs = Input((img_h, img_w, img_ch))
    x = inputs
    for i in range(5):
        if i < 2:
            x = Conv2D(n_filters * (2 ** i), (k, k), strides=(s, s), padding=padding, kernel_regularizer=regularizers.l2(l2_coeff), bias_regularizer=regularizers.l2(l2_coeff))(x)
            x = Activation("relu")(x)
        else:
            x = Conv2D(n_filters * (2 ** i), (k, k), strides=(1, 1), padding=padding, kernel_regularizer=regularizers.l2(l2_coeff), bias_regularizer=regularizers.l2(l2_coeff))(x)
            x = Activation("relu")(x)
        if i != 4:
            x = AveragePooling2D(pool_size=(s, s))(x)
    
    gap = GlobalAveragePooling2D()(x)
    outputs = Dense(out_ch, activation='softmax', kernel_regularizer=regularizers.l2(l2_coeff), bias_regularizer=regularizers.l2(l2_coeff))(gap)
    
    model = Model(inputs, outputs)    
    
    model.compile(optimizer=SGD(lr=0, momentum=0.9, nesterov=True), loss=objectives.categorical_crossentropy, metrics=['accuracy'])

    return model 


def network_gradability_v3(img_shape):
    """
    generate network based on unet
    """    
        
    # set image specifics
    n_filters = 32
    out_ch = 4
    k = 3  # kernel size
    s = 2  # stride
    img_h, img_w, img_ch = img_shape[0], img_shape[1], img_shape[2]
    padding = 'same'
    l2_coeff = 0.0005
    
    inputs = Input((img_h, img_w, img_ch))
    last_layer = inputs
    for i in range(5):
        if i < 2:
            x = selu_block(last_layer, n_filters * (2 ** i), k, s, l2_coeff, padding)
        else:
            x = selu_block(last_layer, n_filters * (2 ** i), k, 1, l2_coeff, padding)
        if i != 4:
            x = MaxPooling2D(pool_size=(s, s))(x)
        last_layer = x
    
    gap = GlobalAveragePooling2D()(last_layer)
    outputs = Dense(out_ch, activation='softmax', kernel_regularizer=regularizers.l2(l2_coeff), bias_regularizer=regularizers.l2(l2_coeff))(gap)
    
    model = Model(inputs, outputs)    
    
    model.compile(optimizer=SGD(lr=0, momentum=0.9, nesterov=True), loss=objectives.categorical_crossentropy, metrics=['accuracy'])

    return model 


def network_gradability_v2(img_shape):
    # set image specifics
    n_filters = 16
    out_ch = 4
    k = 3  # kernel size
    s = 2  # stride
    depth = 3
    img_h, img_w, img_ch = img_shape[0], img_shape[1], img_shape[2]
    padding = 'same'
    blocks = []
    
    inputs = Input((img_h, img_w, img_ch))
    list_out = [inputs]
    # encode
    for index in range(depth):
        conv = conv_block(list_out[-1], n_filters * (2 ** index), (k, k), (1, 1), padding, l2_coeff=0)
        conv = conv_block(conv, n_filters * (2 ** index), (k, k), (s, s), padding, l2_coeff=0)
        list_out.append(conv)
   
    gap = GlobalAveragePooling2D()(list_out[-1])
    outputs = Dense(out_ch, activation='softmax')(gap)
    
    model = Model(inputs, outputs)    
    
    def loss(y_true, y_pred):
        return objectives.categorical_crossentropy(y_true, y_pred)

    model.compile(optimizer=SGD(lr=0, momentum=0.9, nesterov=True), loss=loss, metrics=['accuracy'])

    return model


def network_gradability(img_shape):
    # set image specifics
    n_filters = 16
    out_ch = 4
    k = 3  # kernel size
    s = 2  # stride
    img_h, img_w, img_ch = img_shape[0], img_shape[1], img_shape[2]
    padding = 'same'
    l2_coeff = 0.0005
    list_n_building_blocks = [2, 2, 2, 2]
    blocks = []
    
    inputs = Input((img_h, img_w, img_ch))
    blocks.append(conv_block(inputs, n_filters, (k, k), (s, s), padding, l2_coeff))
    for index, n_building_blocks in enumerate(list_n_building_blocks):
        blocks.append(resnet_blocks(blocks[index], n_building_blocks, (2 ** index) * n_filters, (k, k), padding, l2_coeff))
        blocks[index + 1] = conv_block(blocks[index + 1], 2 ** (index + 1) * n_filters, (k, k), (s, s), padding, l2_coeff)
    
    gap = GlobalAveragePooling2D()(blocks[-1])
    outputs = Dense(out_ch, activation='softmax', kernel_regularizer=regularizers.l2(l2_coeff), bias_regularizer=regularizers.l2(l2_coeff))(gap)
    
    model = Model(inputs, outputs)    
    
    def loss(y_true, y_pred):
        return objectives.categorical_crossentropy(y_true, y_pred)

    model.compile(optimizer=SGD(lr=0, momentum=0.9, nesterov=True), loss=loss, metrics=['accuracy'])

    return model
    
