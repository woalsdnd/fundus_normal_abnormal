import label_provider
import numpy as np
import random
import os
import utils

val_ratio = 0.1

# training settings
systems_train = [1, 2, 3, 4]
path_img_dir_template = "/media/ext/resized/system{}/"
train_filenames_labels = label_provider.labels(systems_train, 2, 2, 3, path_img_dir_template, "findings")
n_data = len(train_filenames_labels)
l = range(n_data)
random.shuffle(l)
n_val = int(val_ratio * n_data)
list_training = l[n_val:]
list_val = l[:n_val]
training_out_path = os.path.join("train_val_index", "training.npy")
val_out_path = os.path.join("train_val_index", "val.npy")
if os.path.exists(training_out_path):
    print "file already exists"
else:
    np.save(training_out_path, list_training)
if os.path.exists(val_out_path):
    print "file already exists"
else:
    np.save(val_out_path, list_val)
