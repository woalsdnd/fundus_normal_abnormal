import utils
import label_provider
import os
from PIL import Image
import matplotlib.cm as cm
import numpy as np
from scipy.misc import imresize
from PIL import ImageFilter

tasks_paths=utils.all_files_under("../img_analyses/")
cases=["TP","FN","FP"]
h_target, w_target=512,512
for task_path in tasks_paths:
    for class_case in cases:
        print "processing {} {}...".format(task_path,class_case)
        img_dir=os.path.join(task_path,class_case)
        activation_dir=os.path.join(task_path,class_case+"_activation")
        out_dir=os.path.join(task_path,class_case+"_overlap")
        if not os.path.isdir(out_dir):
            os.makedirs(out_dir)
        img_filenames=utils.all_files_under(img_dir)
        activation_filenames=utils.all_files_under(activation_dir)
        assert len(img_filenames)==len(activation_filenames)
        for index in range(len(img_filenames)):
            assert os.path.basename(img_filenames[index])+".npy"==os.path.basename(activation_filenames[index])
            img=np.asarray(Image.open(img_filenames[index]))
            activation=np.load(activation_filenames[index])
            heatmap = (activation-np.min(activation))/(np.max(activation)-np.min(activation))
            heatmap=imresize(heatmap,(h_target,w_target),'bilinear')
            heatmap_pil_img=Image.fromarray(heatmap)
            heatmap_pil_img=heatmap_pil_img.filter(ImageFilter.GaussianBlur(32))
            heatmap_blurred=np.asarray(heatmap_pil_img)
            heatmap_blurred=np.uint8(cm.jet(heatmap_blurred)[..., :3] * 255)
            overlapped=(img * 0.7 + heatmap_blurred * 0.3).astype(np.uint8)
            Image.fromarray(overlapped).save(os.path.join(out_dir, os.path.basename(img_filenames[index])))
        
        
