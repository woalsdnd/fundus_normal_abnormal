import utils
import os
import argparse
import time
from keras import backend as K
import sys
import iterator_labels
import models
import label_provider
import numpy as np
from keras.models import model_from_json
from keras.utils.np_utils import to_categorical

# arrange arguments
parser = argparse.ArgumentParser()
parser.add_argument(
    '--batch_size',
    type=int,
    help="size of a batch for each GPU",
    required=True
    )
parser.add_argument(
    '--gpu_index',
    type=int,
    required=True
    )
parser.add_argument(
    '--model_dir',
    type=str,
    required=True
    )
parser.add_argument(
    '--normalization_type',
    type=str,
    required=True
    )
parser.add_argument(
    '--load_model_dir',
    type=str,
    required=False
    )
FLAGS, _ = parser.parse_known_args()

n_epochs = 100

if not os.path.isdir(FLAGS.model_dir):
    os.makedirs(FLAGS.model_dir)

# set systems for data split 
systems_train = [1, 2, 4]
systems_validation = [3]
path_img_dir_template = "/media/ext/resized/system{}/"

# set gpu 
os.environ['CUDA_VISIBLE_DEVICES'] = str(FLAGS.gpu_index)

# training settings
train_filenames_labels = label_provider.labels(systems_train, 1, 3, None, path_img_dir_template, option="gradable_nongradable")
validation_filenames_labels = label_provider.labels(systems_validation, 1, 3, None, path_img_dir_template, option="gradable_nongradable")
weight = utils.class_weight(train_filenames_labels)
train_batch_fetcher = iterator_labels.TrainBatchFetcher(train_filenames_labels, FLAGS.batch_size, weight, FLAGS.normalization_type)
validation_batch_fetcher = iterator_labels.ValidationBatchFetcher(validation_filenames_labels, FLAGS.batch_size, FLAGS.normalization_type)
schedules = {'lr':{'0': 0.001, '50':0.0001, '80':0.00001}}
        
# create network
img_shape = utils.image_shape(validation_filenames_labels.loc[0, "filename"])
if FLAGS.load_model_dir:
    network_file = utils.all_files_under(FLAGS.load_model_dir, extension=".json")
    weight_file = utils.all_files_under(FLAGS.load_model_dir, extension=".h5")
    assert len(network_file) == 1 and len(weight_file) == 1
    with open(network_file[0], 'r') as f:
        network = model_from_json(f.read())
    network.load_weights(weight_file[0])
    network = models.set_optimizer_gradability(network)
else:
    network = models.network_gradable_nongradable(img_shape)
network.summary()
with open(os.path.join(FLAGS.model_dir, "network.json"), 'w') as f:
    f.write(network.to_json())
    
# train the network 
check_train_batch, check_test_batch = True, True
scheduler = utils.Scheduler(schedules, schedules['lr']['0'])
for epoch in range(n_epochs):
    # update step sizes, learning rates
    scheduler.update_steps(epoch)
    K.set_value(network.optimizer.lr, scheduler.get_lr())    

    # train on train set
    losses, accs = [], []
    start_time = time.time()
    n = 0
    for filenames, batch_x, batch_y in train_batch_fetcher():
        batch_y = np.expand_dims(np.array(batch_y), axis=1)
        if check_train_batch:
            utils.plot_imgs((batch_x * 255), "../input_checks/train/imgs")
            check_train_batch = False
        loss, acc = network.train_on_batch(batch_x, batch_y)
        losses += [loss] * len(filenames)
        accs += [acc] * len(filenames)
    print "classification loss: {}".format(np.mean(losses))
    print "classification acc: {}".format(np.mean(accs))
    
    # evaluate on validation set
    pred_labels, true_labels = [], []
    for filenames, batch_x, batch_y in validation_batch_fetcher():
        if check_test_batch:
            utils.plot_imgs((batch_x * 255), "../input_checks/test/imgs")
            check_test_batch = False
        preds = network.predict(batch_x)[:, 0]
        pred_labels += preds.tolist()
        true_labels += batch_y.tolist()
        
    utils.print_stats_binary(true_labels, pred_labels)
    duration = time.time() - start_time
    print "{}th epoch ==> duration : {}".format(epoch, duration)
    sys.stdout.flush()
    
    # save weights
    network.save_weights(os.path.join(FLAGS.model_dir, "weight_{}epoch.h5".format(epoch)))
