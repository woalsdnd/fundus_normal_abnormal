import multiprocessing
import threading
import Queue
from uuid import uuid4

import numpy as np
import SharedArray

import utils

img_h, img_w = 512, 512

def load_shared(args):
    i, img_array_name, mask_array_name, lm_array_name, lms, regions, normalize, augment, fname = args
    img_array = SharedArray.attach(img_array_name)
    mask_array = SharedArray.attach(mask_array_name)
    lm_array = SharedArray.attach(lm_array_name)
    img_array[i], lm_array[i], mask_array[i] = utils.load_augmented(fname, lms, regions, normalize, augment)

def balance_per_class_indices(y, weights):
    weights = np.array(weights, dtype=float)
    p = np.zeros(len(y))
    for i, weight in enumerate(weights):
        p[y == i] = weight
    return np.random.choice(np.arange(len(y)), size=len(y), replace=True,
                            p=np.array(p) / p.sum())

class BatchIterator(object):

    def __call__(self, *args):
        self.X, self.y, self.lm, self.region = args
        return self
        
    def __iter__(self):
        n_samples = self.X.shape[0]
        bs = self.batch_size
        for i in range((n_samples + bs - 1) // bs - 1):
            sl = slice(i * bs, (i + 1) * bs)
            X = self.X[sl]
            y = self.y[sl]
            lm = self.lm[sl]
            region=self.region[sl]
            yield self.transform(X, y, lm, region)

class QueueIterator(BatchIterator):
    def __iter__(self):
        queue = Queue.Queue(maxsize=20)
        end_marker = object()
        def producer():
            for filenames, Xb, yb, lm, mask in super(QueueIterator, self).__iter__():
                queue.put((np.array(filenames), np.array(Xb), np.array(yb), np.array(lm), np.array(mask)))
            queue.put(end_marker)
        
        thread = threading.Thread(target=producer)
        thread.daemon = True
        thread.start()

        item = queue.get()
        while item is not end_marker:
            yield item
            queue.task_done()
            item = queue.get()

class SharedIterator(QueueIterator):
    def __init__(self):
        self.pool = multiprocessing.Pool()

    def transform(self, fnames, labels, lms, regions):
        img_shared_array_name = str(uuid4())
        lm_shared_array_name = str(uuid4())
        mask_shared_array_name = str(uuid4())
        try:
            img_shared_array = SharedArray.create(
                img_shared_array_name, [len(fnames), img_h, img_w, 3], dtype=np.float32)
            lm_shared_array = SharedArray.create(
                lm_shared_array_name, lms.shape, dtype=np.float32)
            mask_shared_array = SharedArray.create(
                mask_shared_array_name, [len(fnames), img_h, img_w], dtype=np.float32)
                                        
            args = []
            
            for i, fname in enumerate(fnames):
                args.append((i, img_shared_array_name, mask_shared_array_name, lm_shared_array_name, lms[i], regions[i],
                              self.normalize, self.augment, fname))

            self.pool.map(load_shared, args)
            imgs = np.array(img_shared_array, dtype=np.float32)
            lm_pts = np.array(lm_shared_array, dtype=np.float32)
            masks = np.array(mask_shared_array, dtype=np.float32)
            
        finally:
            SharedArray.delete(img_shared_array_name)
            SharedArray.delete(lm_shared_array_name)
            SharedArray.delete(mask_shared_array_name)
            
        return fnames, imgs, labels, lm_pts, masks
    
class TrainBatchFetcher(SharedIterator):
    def __init__(self, train_filenames_labels, batch_size, weight, normalize):
        self.train_files = np.array(train_filenames_labels.filename.tolist())
        self.train_labels = np.array(train_filenames_labels.label.tolist())
        # (y_disc, x_disc, y_fovea, x_fovea)
        self.train_lm_pts = np.array(zip(train_filenames_labels.y_disc.tolist(), train_filenames_labels.x_disc.tolist(),
                                 train_filenames_labels.y_fovea.tolist(), train_filenames_labels.x_fovea.tolist()))   
        self.train_regions = utils.codify_regions(train_filenames_labels.region.tolist())
        self.weight = np.array(weight)
        self.normalize = normalize
        self.augment = True
        self.batch_size=batch_size
        super(TrainBatchFetcher, self).__init__()
        
    def __call__(self):
        indices = balance_per_class_indices(self.train_labels, weights=self.weight)
        X = self.train_files[indices]
        y = self.train_labels[indices]
        lm = self.train_lm_pts[indices]
        regions = self.train_regions[indices]
        return super(TrainBatchFetcher, self).__call__(X, y, lm, regions)

class ValidationBatchFetcher(SharedIterator):
    def __init__(self, validation_filenames_labels, batch_size, normalize):
        self.validation_files = np.array(validation_filenames_labels.filename.tolist())
        self.validation_labels = np.array(validation_filenames_labels.label.tolist())
        self.validation_lm_pts = np.array(zip(validation_filenames_labels.y_disc.tolist(), validation_filenames_labels.x_disc.tolist(),
                                 validation_filenames_labels.y_fovea.tolist(), validation_filenames_labels.x_fovea.tolist()))   
        self.validation_regions = utils.codify_regions(validation_filenames_labels.region.tolist())
        self.normalize = normalize
        self.augment = False
        self.batch_size=batch_size
        super(ValidationBatchFetcher, self).__init__()
        
    def __call__(self):
        return super(ValidationBatchFetcher, self).__call__(self.validation_files, self.validation_labels, self.validation_lm_pts, self.validation_regions)

