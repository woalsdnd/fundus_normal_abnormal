import utils
import os
import argparse
import time
from keras import backend as K
import sys
# import iterator
import iterator_v2
import models
import label_provider
import numpy as np

# arrange arguments
parser=argparse.ArgumentParser()
parser.add_argument(
    '--n_epochs',
    type=int,
    help="Number of epochs",
    required=True
    )
parser.add_argument(
    '--batch_size',
    type=int,
    help="size of a batch for each GPU",
    required=True
    )
parser.add_argument(
    '--starting_index_gpu',
    type=int,
    help="GPU index",
    required=True
    )
parser.add_argument(
    '--num_gpus',
    type=int,
    help="number of gpus",
    required=True
    )
parser.add_argument(
    '--model_dir',
    type=str,
    help="output directory",
    required=True
    )
FLAGS,_= parser.parse_known_args()

if not os.path.isdir(FLAGS.model_dir):
    os.makedirs(FLAGS.model_dir)

# set systems for data split 
systems_train=[1,2,4]
systems_validation=[3]
# path_img_dir_template="/media/ext/cropped_around_landmark/system{}/disc/"
path_img_dir_template="/media/ext/resized/system{}/"

# set gpu 
os.environ['CUDA_VISIBLE_DEVICES']=",".join([str(i) for i in xrange(FLAGS.starting_index_gpu,FLAGS.starting_index_gpu+FLAGS.num_gpus)])

# training settings
train_filenames_labels=label_provider.labels(systems_train,2,2,3,path_img_dir_template,"findings")
validation_filenames_labels=label_provider.labels(systems_validation,2,2,3,path_img_dir_template,"findings")
weight=utils.class_weight(train_filenames_labels)
# train_batch_fetcher=iterator.TrainBatchFetcher(train_filenames_labels, FLAGS.batch_size, weight, "data")
# validation_batch_fetcher=iterator.ValidationBatchFetcher(validation_filenames_labels, FLAGS.batch_size, "data")
train_batch_fetcher=iterator_v2.TrainBatchFetcher(train_filenames_labels, FLAGS.batch_size, weight, "data")
validation_batch_fetcher=iterator_v2.ValidationBatchFetcher(validation_filenames_labels, FLAGS.batch_size, "data")
schedules={'lr':{'0': 0.001, '50':0.0001, '80':0.00001}}
        
# create network
img_shape=utils.image_shape(validation_filenames_labels.loc[0,"filename"])
network=models.network_activation_out_light(img_shape)
network.summary()
# save the network
with open(os.path.join(FLAGS.model_dir,"network.json"),'w') as f:
    f.write(network.to_json())
    
# train the network 
scheduler=utils.Scheduler(schedules, schedules['lr']['0'])
check_train_batch=True
check_test_batch=True
for epoch in range(FLAGS.n_epochs):
    # update step sizes, learning rates
    scheduler.update_steps(epoch)
    K.set_value(network.optimizer.lr, scheduler.get_lr())    

    # train on train set
    start_time = time.time()
    for filenames, batch_x, batch_y, batch_lms, batch_mask in train_batch_fetcher():
        for n in range(len(batch_y)):
            batch_mask[n,...]*=batch_y[n]
        if check_train_batch:
            utils.plot_imgs(batch_x, "../input_checks/train/imgs")
            utils.plot_imgs((batch_mask*255), "../input_checks/train/masks")
            check_train_batch=False
        network.train_on_batch(batch_x, [batch_y, np.expand_dims(batch_mask[:,::32,::32], axis=3)])

    # evaluate on validation set
    utils.print_stats(batch_y, network.predict(batch_x)[0])
    pred_labels=[]
    true_labels=[]
    for filenames, batch_x, batch_y, batch_lms, batch_mask in validation_batch_fetcher():
        for n in range(len(batch_y)):
            batch_mask[n,...]*=batch_y[n]
        if check_test_batch:
            utils.plot_imgs(batch_x, "../input_checks/test/imgs")
            utils.plot_imgs((batch_mask*255), "../input_checks/test/masks")
            check_test_batch=False
        pred_labels+=network.predict(batch_x)[0].tolist()
        true_labels+=batch_y.tolist()

    utils.print_stats(true_labels, pred_labels)
    duration=time.time()-start_time
    print "{}th epoch ==> duration : {}".format(epoch, duration)
    sys.stdout.flush()
    
    # save weights
    network.save_weights(os.path.join(FLAGS.model_dir,"weight_{}epoch.h5".format(epoch)))
