import os
import re

from PIL import Image

import numpy as np
import pandas as pd
from sklearn.metrics import confusion_matrix
from sklearn.metrics.ranking import roc_auc_score
import random
from scipy.ndimage.interpolation import rotate
from skimage.transform import warp, AffineTransform
from subprocess import Popen, PIPE
import math
import mask_region
from sklearn.metrics import cohen_kappa_score
from keras.models import model_from_json

eigenvectors = np.array([[-0.82636916, 0.45495284, 0.31731006], [-0.49457364, -0.34820691, -0.79025788], [-0.25108081, -0.81019519, 0.51816088]])
sqrt_eigenvalues = np.array([52.87425275, 20.39352121, 7.13604252])
data_means = (107.08857011, 62.60118861, 32.40658611)
data_stds = (47.41452376, 35.07184695, 29.53423681)


def inference_config():
    default_path_img_dir_template = "/media/ext/resized/system{}/"
    config_dict = {"systems_validation" : [3],
                           "path_img_dir_template" : default_path_img_dir_template}
    return config_dict


def split_training_set(train_filenames_labels):
    training_indices = np.load("train_val_index/training.npy")
    val_indices = np.load("train_val_index/val.npy")
    return train_filenames_labels.iloc[training_indices], train_filenames_labels.iloc[val_indices]
    

def mkdir_if_not_exist(out_dir):
    if not os.path.isdir(out_dir):
        os.makedirs(out_dir)


def analyze_imgs(filenames, true_labels, pred_labels, activations, out_dir):
    assert len(true_labels) == len(pred_labels) and len(filenames) == len(true_labels)
    dir_FN = os.path.join(out_dir, "FN")
    dir_FP = os.path.join(out_dir, "FP")
    dir_TP = os.path.join(out_dir, "TP")
    dir_FN_activation = os.path.join(out_dir, "FN_activation")
    dir_FP_activation = os.path.join(out_dir, "FP_activation")
    dir_TP_activation = os.path.join(out_dir, "TP_activation")
    mkdir_if_not_exist(dir_FN)
    mkdir_if_not_exist(dir_FP)
    mkdir_if_not_exist(dir_TP)
    mkdir_if_not_exist(dir_FN_activation)
    mkdir_if_not_exist(dir_FP_activation)
    mkdir_if_not_exist(dir_TP_activation)
    pred_labels = outputs2labels(pred_labels, 0, 1)
    for i in range(len(filenames)):
        if not (true_labels[i] == 0 and pred_labels[i] == 0):
            if true_labels[i] == 1 and pred_labels[i] == 0:  # FN
                pipes = Popen(["cp", filenames[i], os.path.join(dir_FN, os.path.basename(filenames[i]))],
                                stdout=PIPE, stderr=PIPE)
                np.save(os.path.join(dir_FN_activation, os.path.basename(filenames[i])), activations[i, :, :, 0])
            elif true_labels[i] == 0 and pred_labels[i] == 1:  # FP
                pipes = Popen(["cp", filenames[i], os.path.join(dir_FP, os.path.basename(filenames[i]))],
                                stdout=PIPE, stderr=PIPE)
                np.save(os.path.join(dir_FP_activation, os.path.basename(filenames[i])), activations[i, :, :, 0])
            elif true_labels[i] == 1 and pred_labels[i] == 1:  # TP        
                pipes = Popen(["cp", filenames[i], os.path.join(dir_TP, os.path.basename(filenames[i]))],
                                stdout=PIPE, stderr=PIPE)
                np.save(os.path.join(dir_TP_activation, os.path.basename(filenames[i])), activations[i, :, :, 0])

            std_out, std_err = pipes.communicate()


def analyze_imgs_image_quality(filenames, true_labels, pred_labels, out_dir):
    assert len(true_labels) == len(pred_labels) and len(filenames) == len(true_labels)
    for i in range(len(filenames)):
        if true_labels[i] != pred_labels[i]:
            outdir = os.path.join(out_dir, "true{}_pred{}".format(true_labels[i], pred_labels[i]))
            mkdir_if_not_exist(outdir)
            pipes = Popen(["cp", filenames[i], os.path.join(outdir, os.path.basename(filenames[i]))],
                            stdout=PIPE, stderr=PIPE)
            std_out, std_err = pipes.communicate()


def print_stats_binary(y_true, y_pred):
    y_true = np.array(y_true)
    y_pred = np.array(y_pred)
    if len(y_true[y_true == 1]) > 0:
        cm = confusion_matrix(y_true, outputs2labels(y_pred, 0, 1))
        spe = 1.*cm[0, 0] / (cm[0, 1] + cm[0, 0])
        sen = 1.*cm[1, 1] / (cm[1, 0] + cm[1, 1])
        HM = 2 * spe * sen / (spe + sen)
        AUC_ROC = roc_auc_score(y_true, y_pred)
        print cm
        print "specificity : {},  sensitivity : {}, harmonic mean : {},  ROC_AUC : {} ".format(spe, sen, HM, AUC_ROC)   
    else:
        print "specificity: {}".format(1.*len(y_true))


def vetorize(labels, n_class):           
    # labels : 1d array or list
    n_data = len(labels)
    vectorized_labels = np.zeros((n_data, n_class))
    for i in range(n_data):
        vectorized_labels[i, int(labels[i])] = 1
    return vectorized_labels

        
def all_files_under(path, extension=None, append_path=True, sort=True):
    if append_path:
        if extension is None:
            filenames = [os.path.join(path, fname) for fname in os.listdir(path)]
        else:
            filenames = [os.path.join(path, fname) for fname in os.listdir(path) if fname.endswith(extension)]
    else:
        if extension is None:
            filenames = [os.path.basename(fname) for fname in os.listdir(path)]
        else:
            filenames = [os.path.basename(fname) for fname in os.listdir(path) if fname.endswith(extension)]
    
    if sort:
        filenames = sorted(filenames)
    
    return filenames


def get_mask_of_regions(img_size, disc_center, macula_center, regions):
    region_codes = ["None", "M", "T", "ST", "IT", "SN", "IN", "SD", "ID"]
    mask = np.zeros(img_size)
    for index in range(1, len(regions)):
        if regions[index] == 1:
            mask[(mask == 1) | (mask_region.mask(img_size, disc_center, macula_center, region_codes[index]) == 1)] = 1
    return mask


def load_augmented_fov(fname, normalize, augment):
    # read image file
    img_aug, mask_aug = load_imgs_masks([fname], normalize, augment)
    img_aug = img_aug[0, ...]
    mask_aug = mask_aug[0, ...]
    assert len(img_aug.shape) == 3 and len(mask_aug.shape) == 2
    return img_aug, mask_aug


def load_imgs_lms(filenames, lms, normalize=None, augment=True):
    # filenames, lms : list of arrays
    img_shape = image_shape(filenames[0])
    if len(img_shape) == 3:
        images_arr = np.zeros((len(filenames), img_shape[0], img_shape[1], img_shape[2]), dtype=np.float32)
        ori_images_arr = np.zeros((len(filenames), img_shape[0], img_shape[1], img_shape[2]), dtype=np.float32)
    elif len(img_shape) == 2:
        images_arr = np.zeros((len(filenames), img_shape[0], img_shape[1]), dtype=np.float32)
        ori_images_arr = np.zeros((len(filenames), img_shape[0], img_shape[1], img_shape[2]), dtype=np.float32)
    lm_pts = np.zeros((len(filenames), 4), dtype=np.float32)
    for index, lm in enumerate(lms):
        lm_pts[index, :] = lm
    
    # convert to z score per image
    for file_index in xrange(len(filenames)):
        img = np.array(Image.open(filenames[file_index]))
        # remove low values
        img[img < 10] = 0
        h, w, _ = img.shape

        if augment:
            img[:60, :256, :] = 0  # erase time tag
            # random flip
            if random.getrandbits(1):
                img = img[:, ::-1, :]  # flip an image
                lm_pts[file_index, 1] = w - lm_pts[file_index, 1]  # x_disc
                lm_pts[file_index, 3] = w - lm_pts[file_index, 3]  # x_fovea
           
            # affine transform (translation, scale, shear, rotation)
            r_angle = random.randint(0, 359)
            x_scale, y_scale = random.uniform(1. / 1.15, 1.15), random.uniform(1. / 1.15, 1.15)
            shear = random.uniform(-10 * np.pi / 180, 10 * np.pi / 180)
            x_translation, y_translation = random.randint(0, 20), random.randint(0, 20)
            tform = AffineTransform(scale=(x_scale, y_scale), shear=shear,
                                    translation=(x_translation, y_translation))
            img = warp(img, tform.inverse, output_shape=(img.shape[0], img.shape[1]))
            img *= 255
            img = rotate(img, r_angle, axes=(0, 1), order=1, reshape=False)
            img_color = np.copy(img)

            lm_pts[file_index, 0] = y_scale * lm_pts[file_index, 0] * np.cos(shear) + y_translation
            lm_pts[file_index, 1] = x_scale * lm_pts[file_index, 1] - y_scale * lm_pts[file_index, 0] * np.sin(shear) + x_translation
            lm_pts[file_index, 2] = y_scale * lm_pts[file_index, 2] * np.cos(shear) + y_translation
            lm_pts[file_index, 3] = x_scale * lm_pts[file_index, 3] - y_scale * lm_pts[file_index, 2] * np.sin(shear) + x_translation
            lm_pts[file_index, :2] = rotate_pt((h // 2, w // 2), (lm_pts[file_index, 0], lm_pts[file_index, 1]), r_angle)
            lm_pts[file_index, 2:] = rotate_pt((h // 2, w // 2), (lm_pts[file_index, 2], lm_pts[file_index, 3]), r_angle)
            if normalize == "data":
                # perturb colors
                for i in range(3):
                    img += np.random.normal(0, 1) * sqrt_eigenvalues[i] * eigenvectors[i, ...]
            elif normalize == "instance_mean":
                img = img * (1 + random.random() * 0.1) if random.getrandbits(1) else img * (1 - random.random() * 0.1)
        else:
            img_color = np.copy(img)
    
        ori_images_arr[file_index] = img_color

        if normalize == "instance_std":
            means = np.zeros(3)
            stds = np.array([255.0, 255.0, 255.0])
            for i in range(3):
                if len(img_color[..., i][img_color[..., i] > 10]) > 1:
                    means[i] = np.mean(img_color[..., i][img_color[..., i] > 10])
                    std_val = np.std(img_color[..., i][img_color[..., i] > 10])
                    stds[i] = std_val if std_val > 0 else 255
                
            images_arr[file_index] = (img - means) / stds
        elif normalize == "data_std":
            means = data_means
            stds = data_stds
            images_arr[file_index] = (img - means) / stds
        elif normalize == "instance_mean":
            means = np.zeros(3)
            for i in range(3):
                if len(img_color[..., i][img_color[..., i] > 10]) > 1:
                    means[i] = np.mean(img_color[..., i][img_color[..., i] > 10])
            images_arr[file_index] = img - means
        elif normalize == "normalize":
            images_arr[file_index] = 1.*img / 255.0
        else:
            images_arr[file_index] = img.astype(np.float32)
        
    return ori_images_arr, images_arr, lm_pts


def load_imgs_only(filenames, normalize=None, augment=True):
    # filenames: list of arrays
    img_shape = image_shape(filenames[0])
    if len(img_shape) == 3:
        images_arr = np.zeros((len(filenames), img_shape[0], img_shape[1], img_shape[2]), dtype=np.float32)
    elif len(img_shape) == 2:
        images_arr = np.zeros((len(filenames), img_shape[0], img_shape[1]), dtype=np.float32)
    
    # convert to z score per image
    for file_index in xrange(len(filenames)):
        img = np.array(Image.open(filenames[file_index]))
        # remove low values
        img[img < 10] = 0
        h, w, _ = img.shape
        
        if augment:
            img[:60, :256, :] = 0  # erase time tag
            # random flip
            if random.getrandbits(1):
                img = img[:, ::-1, :]  # flip an image
           
            # affine transform (translation, scale, shear, rotation)
            r_angle = random.randint(0, 359)
            x_scale, y_scale = random.uniform(1. / 1.05, 1.05), random.uniform(1. / 1.05, 1.05)
            shear = random.uniform(-5 * np.pi / 180, 5 * np.pi / 180)
            x_translation, y_translation = random.randint(0, 20), random.randint(0, 20)
            tform = AffineTransform(scale=(x_scale, y_scale), shear=shear,
                                    translation=(x_translation, y_translation))
            img = warp(img, tform.inverse, output_shape=(img.shape[0], img.shape[1]))
            img *= 255
            img = rotate(img, r_angle, axes=(0, 1), order=1, reshape=False)
            img_color = np.copy(img)
        else:
            img_color = np.copy(img)

        if "instance" in normalize:
            means = np.zeros(3)
            stds = np.array([255.0, 255.0, 255.0])
            for i in range(3):
                if len(img_color[..., i][img_color[..., i] > 10]) > 1:
                    means[i] = np.mean(img_color[..., i][img_color[..., i] > 10])
                    std_val = np.std(img_color[..., i][img_color[..., i] > 10])
                    stds[i] = std_val if std_val > 0 else 255
                
            images_arr[file_index] = (img - means) / stds
        elif normalize == "data":
            means = data_means
            stds = data_stds
            images_arr[file_index] = (img - means) / stds
        elif normalize == "normalize":
            images_arr[file_index] = 1.*img / 255.0
        else:
            images_arr[file_index] = img.astype(np.float32)
        
    return images_arr


def load_augmented_img(fname, normalize, augment):
    img_aug = load_imgs_only([fname], normalize, augment)[0, ...]
    assert len(img_aug.shape) == 3
    return img_aug


def load_augmented(fname, lms, regions, normalize, augment):
    ori_img, img_aug, lms_aug = load_imgs_lms([fname], [lms], normalize, augment)
    ori_img, img_aug, lms_aug = ori_img[0, ...], img_aug[0, ...], lms_aug[0, ...]

    region_mask = get_mask_of_regions((img_aug.shape[:2]), lms_aug[:2], lms_aug[2:], regions)
    region_mask[ori_img[..., 0] < 10] = 0
    
    if normalize == "instance_intensity":
        normalized_img = ori_img / 255
        img_aug = np.concatenate([img_aug, normalized_img], axis=2)
    
    assert len(img_aug.shape) == 3 and lms_aug.shape == (4,) and len(region_mask.shape) == 2
    return img_aug, lms_aug, region_mask


def val2cat(vals, n_cat):
    tensor = np.zeros((vals.shape[0], n_cat))
    for index, v in enumerate(vals):
        tensor[index, int(v)] = 1
    return tensor

# def class_weight_from_file(label_file, label_type):
#     df_train=pd.read_csv(label_file)
#     data_ratio=df_train[label_type].value_counts(normalize=True)
#     weight=np.array([1./data_ratio[0], 1./data_ratio[1]])
#     weight/=sum(weight)
#     print "class weight : {}".format(weight)
#     return weight


def class_weight(df):
    data_ratio = df["label"].value_counts(normalize=True)
    weight = np.array([1. / r for r in data_ratio])
    weight /= sum(weight)
    print "data number"
    print df["label"].value_counts(normalize=False)
    print "class weight : {}".format(weight)
    return weight


def class_weight_float(df):
    data_ratio = df["label"].value_counts(normalize=True)
    weight = np.array([1. / data_ratio[0], 1. / np.sum(data_ratio[1:])])
    weight /= sum(weight)
    print "data number"
    print df["label"].value_counts(normalize=False)
    print "class weight : {}".format(weight)
    return weight


def class_weight_category(label_file, label_type):
    df_train = pd.read_csv(label_file)
    df_train = df_train[df_train[label_type] != -1]
    data_ratio = df_train[label_type].value_counts(normalize=True)
    weight = 1. / data_ratio
    weight /= sum(weight)
    print "class weight : {}".format(weight)
    return weight


def image_shape(filename):
    img = Image.open(filename)
    img_arr = np.asarray(img)
    img_shape = img_arr.shape
    return img_shape


def codify_regions(list_region):
    # input : list of strings of regions
    # output : numpy matrix of codified regions
    n_regions = 8
    region_code = {"":0, "M":1, "T":2, "ST":3, "IT":4, "SN":5, "IN":6, "SD":7, "ID":8}
    codified_regions = np.zeros((len(list_region), n_regions + 1))
    for index, region_str in enumerate(list_region):
        for region in [chunk.strip() for chunk in region_str.split("|")]:
            codified_regions[index, region_code[region]] = 1
    return codified_regions


def rotate_pt(origin, point, angle):
    """
    Rotate a point counterclockwise by a given angle around a given origin.
    """
    rad = angle * math.pi / 180
    oy, ox = origin
    py, px = point
    qx = ox + math.cos(rad) * (px - ox) + math.sin(rad) * (py - oy)
    qy = oy - math.sin(rad) * (px - ox) + math.cos(rad) * (py - oy)
    return qy, qx


def load_imgs_masks(filenames, normalize=None, augment=True):
    img_shape = image_shape(filenames[0])
    images_arr = np.zeros((len(filenames), img_shape[0], img_shape[1], img_shape[2]), dtype=np.float32)
    masks_arr = np.zeros((len(filenames), img_shape[0], img_shape[1]), dtype=np.float32)
    
    # convert to z score per image
    for file_index in xrange(len(filenames)):
        img = np.array(Image.open(filenames[file_index]))
        h, w, _ = img.shape

        if augment:
            # random flip
            if random.getrandbits(1):
                img = img[:, ::-1, :]  # flip an image
           
            # affine transform (translation, scale, shear, rotation)
            r_angle = random.randint(0, 359)
            x_scale, y_scale = random.uniform(1. / 1.15, 1.15), random.uniform(1. / 1.15, 1.15)
            shear = random.uniform(-10 * np.pi / 180, 10 * np.pi / 180)
            x_translation, y_translation = random.randint(0, 20), random.randint(0, 20)
            tform = AffineTransform(scale=(x_scale, y_scale), shear=shear,
                                    translation=(x_translation, y_translation))
            img = warp(img, tform.inverse, output_shape=(img.shape[0], img.shape[1]))
            img *= 255
            img = rotate(img, r_angle, axes=(0, 1), order=1, reshape=False)
            aug_img = np.copy(img)
            # perturb colors
#             for i in range(3):
#                 img+=np.random.normal(0, 1)*sqrt_eigenvalues[i]*eigenvectors[i,...]

        # normalize the image
        if normalize == "instance":
            means = np.zeros(3)
            stds = np.zeros(3)
            for i in range(3):
                if len(aug_img[..., i][aug_img[..., i] > 10]) > 0:
                    means[i] = np.mean(aug_img[..., i][aug_img[..., i] > 10])
                    stds[i] = np.std(aug_img[..., i][aug_img[..., i] > 10])
                else:
                    stds[i] = 255
            images_arr[file_index] = (img - means) / stds
        elif normalize == "data":
            means = data_means
            stds = data_stds
            images_arr[file_index] = (img - means) / stds
        else:
            images_arr[file_index] = img.astype(np.float32)
        
        # remove low values
        if augment:
            images_arr[file_index][aug_img < 10] = 0
            masks_arr[file_index][aug_img[..., 0] > 10] = 1
        else:
            images_arr[file_index][img < 10] = 0
            masks_arr[file_index][img[..., 0] > 10] = 1
        
    return images_arr, masks_arr


def print_metrics(itr, **kargs):
    print "*** Round {}  ====> ".format(itr),
    for name, value in kargs.items():
        print ("{} : {}, ".format(name, value)),
    print ""

 
def load_labels(label_file, label_type):
    df = pd.read_csv(label_file)
    if label_type == 'abnormal':
        return dict(zip(df.filename, df.abnormal))
    elif label_type == 'ungradable':
        return dict(zip(df.filename, df.ungradable))                    
    else:
        raise Exception("Unknown label type : {}".format(label_type))

# def validation_inputs(files, normalize):
#     n_total=sum([len(files[i]) for i in xrange(len(files))])
#     img_shape=image_shape(files[0][0])
#     labels=[]
#     imgs=np.empty((n_total, img_shape[0], img_shape[1], img_shape[2]))
#     n=0
#     for label in xrange(len(files)):
#         n_imgs=len(files[label])
#         labels+=[label]*n_imgs
#         imgs[n:n+n_imgs,...]=load_imgs_lms(files[label], normalize)
#         n+=n_imgs
#             
#     return imgs, labels


def outputs2labels(outputs, min_val, max_val):
    return np.clip(np.round(outputs), min_val, max_val)

    
def check_labels(filepaths, labels, label_type):
    label_dict = load_labels("labels_train.csv", label_type)
    for index, filepath in enumerate(filepaths):  
        label = label_dict[re.sub(".tiff" + "$", "", os.path.basename(filepath))]
        assert label == labels[index]


def load_network(dir_name, trainable=False):
    network_file = all_files_under(dir_name, extension=".json")
    weight_file = all_files_under(dir_name, extension=".h5")
    assert len(network_file) == 1 and len(weight_file) == 1
    with open(network_file[0], 'r') as f:
        network = model_from_json(f.read())
    network.load_weights(weight_file[0])
    network.trainable = trainable
    for l in network.layers:
        l.trainable = trainable
    return network


def print_stats(y_true, y_pred):
    cm = confusion_matrix(y_true, outputs2labels(y_pred, 0, 1))
    spe = 1.*cm[0, 0] / (cm[0, 1] + cm[0, 0])
    sen = 1.*cm[1, 1] / (cm[1, 0] + cm[1, 1])
    f1 = 2 * spe * sen / (spe + sen)
    AUC_ROC = roc_auc_score(y_true, y_pred)
    
    print cm
    print "specificity : {},  sensitivity : {}, F1 : {},  ROC_AUC : {} ".format(spe, sen, f1, AUC_ROC)   


def plot_imgs(imgs, out_dir):
    if not os.path.isdir(out_dir):
        os.makedirs(out_dir)
    for i in range(imgs.shape[0]):
        Image.fromarray(imgs[i, ...].astype(np.uint8)).save(os.path.join(out_dir, "imgs_{}.png".format(i + 1)))


def print_stats_category(y_true, y_pred):
    cm = confusion_matrix(y_true, np.round(np.clip(y_pred, 0, 3))) 
    print cm
    
#     cohen_kappa=cohen_kappa_score(y_true, y_pred, weights="quadratic")
#     print "quadratic kappa score: {}".format(cohen_kappa)

        
class Scheduler:

    def __init__(self, schedules, init_lr):
        self.schedules = schedules
        self.lr = init_lr

    def get_lr(self):
        return self.lr
        
    def update_steps(self, n_round):
        key = str(n_round)
        if key in self.schedules['lr']:
            self.lr = self.schedules['lr'][key]


def labels_for_files(filepaths, img_format, label_file, label_type):
    """
    exclude files not in the label
    """
    label_dict = load_labels(label_file, label_type)
    
    n_missing = 0    
    new_filepaths = []
    label_list = []
    for filepath in filepaths:
        fname = re.sub(img_format + "$", "", os.path.basename(filepath))
        if fname in label_dict and label_dict[fname] != -1:
            label_list.append(label_dict[fname])
            new_filepaths.append(filepath)
        else:
            n_missing += 1
            
    print "missing labels : {}".format(n_missing)
    return new_filepaths, label_list
